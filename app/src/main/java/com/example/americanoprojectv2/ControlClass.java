package com.example.americanoprojectv2;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ControlClass {

    //Variables that holds information about number of players and match score.
    private static int numOfPlayers;
    private static int matchScore;
    //List of players to keep track of the score for each player
    private static List<Player> playerList = new ArrayList<Player>(){};

    //Constructor - Only used once/ only one object created to set the information the user has choose.
    public ControlClass(int numOfPlayersIn, int matchScoreIn, List<String> tempPlayerListIn){
        ControlClass.numOfPlayers = numOfPlayersIn;
        ControlClass.matchScore = matchScoreIn;

        //Adds a new player for each String in tempPlayerList recieved from user input. Randomize the input to hinder gameMaster to "control" the schedule.
        Random random = new Random();
        List<Integer> addedPlayers = new ArrayList<>();
        boolean checkIfUsed = true;
        int randomPlayerIndex = 0;

        for(int i = 0; i < tempPlayerListIn.size(); i++){
            checkIfUsed = true;
            while(checkIfUsed){
                randomPlayerIndex = random.nextInt(tempPlayerListIn.size());
                checkIfUsed = false;
                for (int j : addedPlayers) {
                    if(j == randomPlayerIndex){
                        checkIfUsed = true;
                        break;
                    }
                }
            }
            playerList.add(new Player(tempPlayerListIn.get(randomPlayerIndex)));
            addedPlayers.add(randomPlayerIndex);
        }
    }

    //Method for adding score to player
    public static void addScore(int playerIndex, int score){
        playerList.get(playerIndex).addPlayerScore(score);
    }

    public static List<Player> getPlayerList() {
        return playerList;
    }

    //Gets specific playerObject
    public static Player getPlayer(int indexIn) {
        return playerList.get(indexIn);
    };

    //Getters for numOfPlayers and matchScore
    public static int getNumOfPlayers() {
        return numOfPlayers;
    }
    public static int getMatchScore() {
        return matchScore;
    }



    //Arrays that holds player scheduele - AmTOf4P1 = Americano Tournament Of 4 Court 1 PlayerPosition 1
    //Index equals round - Index 0 = round 1. Int equals specific player from playerList in ControlClass.
    //4player Americano
    static final int[] AmTOf4C1PP1 = new int[]{0, 0, 0};
    static final int[] AmTOf4C1PP2 = new int[]{1, 2, 3};
    static final int[] AmTOf4C1PP3 = new int[]{2, 3, 2};
    static final int[] AmTOf4C1PP4 = new int[]{3, 1, 1};

    //5player Americano
    static final int[] AmTOf5C1PP1 = new int[]{0, 4, 0, 4, 2};
    static final int[] AmTOf5C1PP2 = new int[]{1, 3, 3, 0, 0};
    static final int[] AmTOf5C1PP3 = new int[]{2, 2, 2, 1, 1};
    static final int[] AmTOf5C1PP4 = new int[]{3, 1, 4, 3, 4};

    //8player Americano
    static final int[] AmTOf8C1PP1 = new int[]{0, 0, 1, 0, 1, 1, 2};
    static final int[] AmTOf8C1PP2 = new int[]{1, 2, 2, 4, 4, 7, 5};
    static final int[] AmTOf8C1PP3 = new int[]{2, 4, 5, 1, 3, 2, 3};
    static final int[] AmTOf8C1PP4 = new int[]{3, 6, 6, 5, 6, 4, 4};

    static final int[] AmTOf8C2PP1 = new int[]{4, 1, 0, 2, 0, 0, 0};
    static final int[] AmTOf8C2PP2 = new int[]{5, 3, 3, 6, 5, 6, 7};
    static final int[] AmTOf8C2PP3 = new int[]{6, 5, 4, 3, 2, 3, 1};
    static final int[] AmTOf8C2PP4 = new int[]{7, 7, 7, 7, 7, 5, 6};

    //12player Americano
    static final int[] AmTOf12C1PP1 = new int[]{4, 3, 0, 2, 1, 6, 0, 10, 4, 0, 8};
    static final int[] AmTOf12C1PP2 = new int[]{6, 11, 8, 4, 11, 9, 2, 11, 7, 9, 11};
    static final int[] AmTOf12C1PP3 = new int[]{8, 4, 5, 6, 2, 3, 4, 0, 1, 2, 4};
    static final int[] AmTOf12C1PP4 = new int[]{9, 10, 9, 7, 8, 7, 5, 6, 5, 3, 9};

    static final int[] AmTOf12C2PP1 = new int[]{0, 5, 1, 9, 3, 8, 7, 1, 6, 5, 2};
    static final int[] AmTOf12C2PP2 = new int[]{11, 8, 10, 11, 6, 10, 11, 4, 8, 11, 10};
    static final int[] AmTOf12C2PP3 = new int[]{1, 2, 3, 5, 0, 1, 3, 2, 0, 1, 0};
    static final int[] AmTOf12C2PP4 = new int[]{7, 6, 4, 10, 4, 2, 8, 9, 10, 6, 7};

    static final int[] AmTOf12C3PP1 = new int[]{2, 7, 6, 0, 5, 4, 1, 3, 2, 7, 1};
    static final int[] AmTOf12C3PP2 = new int[]{5, 9, 11, 3, 7, 11, 9, 5, 11, 10, 3};
    static final int[] AmTOf12C3PP3 = new int[]{3, 0, 2, 1, 9, 0, 6, 7, 3, 4, 5};
    static final int[] AmTOf12C3PP4 = new int[]{10, 1, 7, 8, 10, 5, 10, 8, 9, 8, 6};
}
