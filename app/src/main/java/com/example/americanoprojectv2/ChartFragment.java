package com.example.americanoprojectv2;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChartFragment extends Fragment {

    //Creates variables for all the view we will need to access
    LinearLayout llContainer;

    public ChartFragment(){}

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Sets the views/the layout to their specific variable.
        llContainer = view.findViewById(R.id.ll_chart_linearContainer);

        //Updates chartText with updateChart method.
        //chartText.setText(updateChart(ControlClass.getPlayerList()));
        updateChart(ControlClass.getPlayerList());
    }

    @Override
    public void onResume() {
        super.onResume();
        updateChart(ControlClass.getPlayerList());
    }

    //Method to update playerChart. Dont want to change the original list because the playscheme is dependent of it. First create a new templist then return sorted playerinformation in a string.
    public void updateChart(List<Player> tempListIn){
        List<Player> tempList = new ArrayList<Player>();
        for (Player p : tempListIn){
            tempList.add(p);
        }
        Collections.sort(tempList);
        Collections.reverse(tempList);

        llContainer.removeAllViews();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        for (Player pl : tempList){
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.ll_chart_linearContainer, PlayerFragment.newInstance(pl.getName(), pl.getPlayerScore()), pl.getName());
            fragmentTransaction.commit();
        }
    }
}
