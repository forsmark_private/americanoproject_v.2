package com.example.americanoprojectv2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import org.jetbrains.annotations.NotNull;

public class CurrentRoundFragment extends Fragment {

    //Creates variables for all the view we will need to access
    LinearLayout matchContainer;

    MatchFragment matchFragmentCourt1;
    MatchFragment matchFragmentCourt2;
    MatchFragment matchFragmentCourt3;

    public CurrentRoundFragment(){}

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        matchFragmentCourt1 = new MatchFragment();
        matchFragmentCourt2 = new MatchFragment();
        matchFragmentCourt3 = new MatchFragment();
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_current_round, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Sets the views/the layout to their specific variable
        matchContainer = view.findViewById(R.id.ll_currentRound_container);

        createMatchFragmentsBasedOfNumberOfPlayers();
    }

    public void createMatchFragmentsBasedOfNumberOfPlayers(){

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setReorderingAllowed(true);

        if(ControlClass.getNumOfPlayers() == 4 || ControlClass.getNumOfPlayers() == 5){
            fragmentTransaction.add(R.id.ll_currentRound_container, matchFragmentCourt1, "court1");
            fragmentTransaction.commit();
        }
        else if(ControlClass.getNumOfPlayers() == 8){
            fragmentTransaction.add(R.id.ll_currentRound_container, matchFragmentCourt1, "court1");
            fragmentTransaction.add(R.id.ll_currentRound_container, matchFragmentCourt2, "court2");
            fragmentTransaction.commit();
        }
        else if(ControlClass.getNumOfPlayers() == 12){
            fragmentTransaction.add(R.id.ll_currentRound_container, matchFragmentCourt1, "court1");
            fragmentTransaction.add(R.id.ll_currentRound_container, matchFragmentCourt2, "court2");
            fragmentTransaction.add(R.id.ll_currentRound_container, matchFragmentCourt3, "court3");
            fragmentTransaction.commit();
        }
        else {}
    }

}
