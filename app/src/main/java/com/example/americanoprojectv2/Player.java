package com.example.americanoprojectv2;

//Class: One object for each player. Keeps track of player score and returns playerName & playerScore
public class Player implements Comparable<Player> {

    //Variables with information specific for a single player
    private String playerName;
    private int playerScore;

    //Constructor that sets players name and initial score to 0.
    public Player(String playerNameIn){
        this.playerName = playerNameIn;
        playerScore = 0;
    }

    //Getter method for playerName
    public String getName() {
        return playerName;
    }

    //Getter method for playerScore
    public Integer getPlayerScore() { return playerScore; }

    //Method for playerScore - Adds playerScoreIn to playerScore
    public void addPlayerScore(int playerScoreIn) {
        this.playerScore += playerScoreIn;
    }

    //Method to return formatted playerName & playerScore
    public String getPlayerInformation(){
        return String.format("%-15s : %d \n", playerName, playerScore);
    }

    @Override
    public int compareTo(Player u){
        return getPlayerScore().compareTo(u.getPlayerScore());
    }
}
