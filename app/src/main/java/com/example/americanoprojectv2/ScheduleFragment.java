package com.example.americanoprojectv2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import org.jetbrains.annotations.NotNull;

public class ScheduleFragment extends Fragment {

    //Creates variables for all the view we will need to access
    LinearLayout scheduleContainer;

    public ScheduleFragment(){}

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_schedule, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Sets the views/the layout to their specific variable.
        scheduleContainer = view.findViewById(R.id.ll_schedule_container);

        setSchedule();
    }

    public void setSchedule(){
        String r = "";
        String c = "";

        if(ControlClass.getNumOfPlayers() == 4) {
            for (int i = 0; i < ControlClass.AmTOf4C1PP1.length; i++) {
                r = "";
                c = "";

                r += "Round: " + (i + 1) + "\n";
                c += "Court 1: \n";
                c += ControlClass.getPlayer(ControlClass.AmTOf4C1PP1[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf4C1PP2[i]).getName();
                c += "     VS.    ";
                c += ControlClass.getPlayer(ControlClass.AmTOf4C1PP3[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf4C1PP4[i]).getName() + "\n" + "\n";

                FragmentManager fragmentManager = getChildFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.ll_schedule_container, RoundSchedule.newInstance(r, c), "C1");
                fragmentTransaction.commit();
            }
        }
        else if(ControlClass.getNumOfPlayers() == 5) {
            for (int i = 0; i < ControlClass.AmTOf5C1PP1.length; i++) {
                r = "";
                c = "";

                r += "Round: " + (i + 1) + "\n";
                c += "Court 1: \n";
                c += ControlClass.getPlayer(ControlClass.AmTOf5C1PP1[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf5C1PP2[i]).getName();
                c += "     VS.    ";
                c += ControlClass.getPlayer(ControlClass.AmTOf5C1PP3[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf5C1PP4[i]).getName() + "\n" + "\n";

                FragmentManager fragmentManager = getChildFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.ll_schedule_container, RoundSchedule.newInstance(r, c));
                fragmentTransaction.commit();
            }
        }
        else if(ControlClass.getNumOfPlayers() == 8) {
            for (int i = 0; i < ControlClass.AmTOf8C1PP1.length; i++) {
                r = "";
                c = "";

                r += "Round: " + (i + 1) + "\n";
                c += "Court 1: \n";
                c += ControlClass.getPlayer(ControlClass.AmTOf8C1PP1[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf8C1PP2[i]).getName();
                c += "     VS.    ";
                c += ControlClass.getPlayer(ControlClass.AmTOf8C1PP3[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf8C1PP4[i]).getName() + "\n" + "\n";

                c += "Court 2: \n";
                c += ControlClass.getPlayer(ControlClass.AmTOf8C2PP1[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf8C2PP2[i]).getName();
                c += "     VS.    ";
                c += ControlClass.getPlayer(ControlClass.AmTOf8C2PP3[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf8C2PP4[i]).getName() + "\n" + "\n";

                FragmentManager fragmentManager = getChildFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.ll_schedule_container, RoundSchedule.newInstance(r, c));
                fragmentTransaction.commit();
            }
        }
        else if(ControlClass.getNumOfPlayers() == 12) {
            for (int i = 0; i < ControlClass.AmTOf12C1PP1.length; i++) {
                r = "";
                c = "";

                r += "Round: " + (i + 1) + "\n";
                c += "Court 1: \n";
                c += ControlClass.getPlayer(ControlClass.AmTOf12C1PP1[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf12C1PP2[i]).getName();
                c += "     VS.    ";
                c += ControlClass.getPlayer(ControlClass.AmTOf12C1PP3[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf12C1PP4[i]).getName() + "\n" + "\n";

                c += "Court 2: \n";
                c += ControlClass.getPlayer(ControlClass.AmTOf12C2PP1[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf12C2PP2[i]).getName();
                c += "     VS.    ";
                c += ControlClass.getPlayer(ControlClass.AmTOf12C2PP3[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf12C2PP4[i]).getName() + "\n" + "\n";

                c += "Court 3: \n";
                c += ControlClass.getPlayer(ControlClass.AmTOf12C3PP1[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf12C3PP2[i]).getName();
                c += "     VS.    ";
                c += ControlClass.getPlayer(ControlClass.AmTOf12C3PP3[i]).getName() + " & " + ControlClass.getPlayer(ControlClass.AmTOf12C3PP4[i]).getName() + "\n" + "\n";

                FragmentManager fragmentManager = getChildFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.ll_schedule_container, RoundSchedule.newInstance(r, c));
                fragmentTransaction.commit();
            }
        }
    }

}
