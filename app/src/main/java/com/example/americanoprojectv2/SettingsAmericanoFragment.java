package com.example.americanoprojectv2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SettingsAmericanoFragment extends Fragment {

    //Variables that holds information about number of players and match score and will be sent to GameActivity.
    int numOfPlayers;
    int matchScore;

    //Variables to control functions
    //If user chooses X amount of players we need to add X amount of players.
    int numOfPlayersAdded = 0;

    //List that holds information about all the players that will participate in the tournament.
    //Will be sent to GameActivity.
    List<String> tempPlayerList = new ArrayList<String>();

    //Creates variables for all the view we will need to access
    Button fourPlayersBtn;
    Button fivePlayersBtn;
    Button eightPlayersBtn;
    Button twelvePlayersBtn;
    Button addPlayerBtn;
    Button startGameBtn;

    RadioButton sixteenPointRb;
    RadioButton twentyfourPointRb;
    RadioButton thirtytwoPointRb;

    EditText playerNameText;

    //Inflates/sets the XML file to be the fragments layout.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings_americano, container, false);
    }

    //After layout is inflated -> Do the stuff in this method
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Sets the views/the layout to their specific variable.
        fourPlayersBtn = view.findViewById(R.id.btn_settingsAmericano_choose4Players);
        fivePlayersBtn = view.findViewById(R.id.btn_settingsAmericano_choose5Players);
        eightPlayersBtn = view.findViewById(R.id.btn_settingsAmericano_choose8Players);
        twelvePlayersBtn = view.findViewById(R.id.btn_settingsAmericano_choose12Players);
        addPlayerBtn = view.findViewById(R.id.btn_settingsAmericano_addPlayer);
        startGameBtn = view.findViewById(R.id.btn_settingsAmericano_startGame);

        sixteenPointRb = view.findViewById(R.id.rb_settingsAmericano_16pInRGMatchScore);
        twentyfourPointRb = view.findViewById(R.id.rb_settingsAmericano_24pInRGMatchScore);
        thirtytwoPointRb = view.findViewById(R.id.rb_settingsAmericano_32pInRGMatchScore);

        playerNameText = view.findViewById(R.id.et_settingsAmericano_addPlayerName);

        //Disables the possibility to add any text or press any button except for the buttons to choose number of players.
        addPlayerBtn.setEnabled(false);
        startGameBtn.setEnabled(false);
        sixteenPointRb.setEnabled(false);
        twentyfourPointRb.setEnabled(false);
        thirtytwoPointRb.setEnabled(false);
        playerNameText.setEnabled(false);

        //region Listeners(Buttons) for choosing number of players. Changes the int:numOfPlayers and enables the next steps. Highlights pressed button.
        fourPlayersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumOfPlayers(4);
                fourPlayersBtn.setAlpha(1f);
                fivePlayersBtn.setAlpha(0.5f);
                eightPlayersBtn.setAlpha(0.5f);
                twelvePlayersBtn.setAlpha(0.5f);
                addPlayerBtn.setEnabled(true);
                playerNameText.setEnabled(true);
            }
        });
        fivePlayersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumOfPlayers(5);
                fourPlayersBtn.setAlpha(0.5f);
                fivePlayersBtn.setAlpha(1f);
                eightPlayersBtn.setAlpha(0.5f);
                twelvePlayersBtn.setAlpha(0.5f);
                addPlayerBtn.setEnabled(true);
                playerNameText.setEnabled(true);
            }
        });
        eightPlayersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumOfPlayers(8);
                fourPlayersBtn.setAlpha(0.5f);
                fivePlayersBtn.setAlpha(0.5f);
                eightPlayersBtn.setAlpha(1f);
                twelvePlayersBtn.setAlpha(0.5f);
                addPlayerBtn.setEnabled(true);
                playerNameText.setEnabled(true);
            }
        });
        twelvePlayersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumOfPlayers(12);
                fourPlayersBtn.setAlpha(0.5f);
                fivePlayersBtn.setAlpha(0.5f);
                eightPlayersBtn.setAlpha(0.5f);
                twelvePlayersBtn.setAlpha(1f);
                addPlayerBtn.setEnabled(true);
                playerNameText.setEnabled(true);
            }
        });
        //endregion

        //region Listener(Button). Need same amount of presses as numberOfPlayers to enable next step. Each press adds new playerName to String list. First press disables the possibility to change number of players.
        addPlayerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fourPlayersBtn.setEnabled(false);
                fivePlayersBtn.setEnabled(false);
                eightPlayersBtn.setEnabled(false);
                twelvePlayersBtn.setEnabled(false);

                tempPlayerList.add(playerNameText.getText().toString());
                addOneToNumOfPlayersAdded();
                playerNameText.setText(null);

                if(getNumOfPlayers() == getNumOfPlayersAdded()){
                    playerNameText.setEnabled(false);
                    addPlayerBtn.setEnabled(false);
                    sixteenPointRb.setEnabled(true);
                    twentyfourPointRb.setEnabled(true);
                    thirtytwoPointRb.setEnabled(true);
                }
            }
        });
        //endregion

        //region Listeners(RB) to chose matchscore. Sends to activity_game. Enables startGameBtn button.
        sixteenPointRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGameBtn.setEnabled(true);
                setMatchScore(16);
            }
        });
        twentyfourPointRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGameBtn.setEnabled(true);
                setMatchScore(24);
            }
        });
        thirtytwoPointRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGameBtn.setEnabled(true);
                setMatchScore(32);
            }
        });
        //endregion

        //region Listener(Button) for starting the game. Last step in settings. Sends tempPlayerList/numOfPlayers and matchScore to GameActivity.
        startGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GameActivityV2.class);
                intent.putExtra("playerListExtra", (Serializable) tempPlayerList);
                intent.putExtra("numberOfPlayersExtra", numOfPlayers);
                intent.putExtra("chosenScoreExtra", matchScore);
                startActivity(intent);
            }
        });
        //endregion

    }

    //Getters and setters for information variables
    public void setNumOfPlayers(int numOfPlayersIn){
        this.numOfPlayers = numOfPlayersIn;
    }
    public void setMatchScore(int matchScoreIn) { this.matchScore = matchScoreIn; }
    public int getMatchScore() { return matchScore; }
    public int getNumOfPlayers() { return numOfPlayers; }
    public int getNumOfPlayersAdded() { return numOfPlayersAdded; }
    public void addOneToNumOfPlayersAdded() { numOfPlayersAdded++; }

}