package com.example.americanoprojectv2;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public class GameActivityV2 extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager2 pager2;
    FragmentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_v2);

        //Receives the extra information from previous activity and stores it into variables and lists.
        //Will be sent to ControlClass thru constructor.
        int matchScore = getIntent().getIntExtra("chosenScoreExtra", 0);
        int numOfPlayers = getIntent().getIntExtra("numberOfPlayersExtra", 0);
        List<String> tempPlayerList =  (List<String>) getIntent().getSerializableExtra("playerListExtra");

        //Creates a ControlClass that will keep necessary information about the tournament in static variables. Fragments/tabs in GameActivity will then collect information from ControlClass.
        ControlClass controlClass = new ControlClass(numOfPlayers, matchScore, tempPlayerList);

        tabLayout = findViewById(R.id.tabLayout_game_gameSpecificTabs);
        pager2 = findViewById(R.id.viewPager2_game);

        FragmentManager fm = getSupportFragmentManager();
        adapter = new FragmentAdapter(fm, getLifecycle());
        pager2.setAdapter(adapter);

        tabLayout.addTab(tabLayout.newTab().setText("Current Round"));
        tabLayout.addTab(tabLayout.newTab().setText("Chart"));
        tabLayout.addTab(tabLayout.newTab().setText("Schedule"));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        pager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });

    }
}