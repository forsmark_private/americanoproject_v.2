package com.example.americanoprojectv2;

import android.os.Bundle;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.jetbrains.annotations.NotNull;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RoundSchedule#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RoundSchedule extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "roundTextParam";
    private static final String ARG_PARAM2 = "courtNPlayersTextParam";

    // TODO: Rename and change types of parameters
    private String roundText;
    private String courtNPlayersText;

    public RoundSchedule() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * //@param param1 Parameter 1.
     * //@param param2 Parameter 2.
     * @return A new instance of fragment RoundSchedule.
     */
    // TODO: Rename and change types and number of parameters
    public static RoundSchedule newInstance(String roundTextParam, String courtNPlayersTextParam) {
        RoundSchedule fragment = new RoundSchedule();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, roundTextParam);
        args.putString(ARG_PARAM2, courtNPlayersTextParam);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            roundText = getArguments().getString(ARG_PARAM1);
            courtNPlayersText = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_round_schedule, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tvRoundText = view.findViewById(R.id.tv_roundSchedule_roundText);
        tvRoundText.setText(roundText);
        TextView tvCourtNPlayersText = view.findViewById(R.id.tv_roundSchedule_courtNPlayers);
        tvCourtNPlayersText.setText(courtNPlayersText);
    }
}