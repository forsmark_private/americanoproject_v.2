package com.example.americanoprojectv2;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import org.jetbrains.annotations.NotNull;

import static java.lang.Integer.parseInt;


public class MatchFragment extends Fragment {

    //Variable that keeps track of roundNo.
    int roundNo = 0;

    //Creates variables for all the view we will need to access
    TextView headingRound;
    TextView player1;
    TextView player2;
    TextView player3;
    TextView player4;
    EditText team1Score;
    EditText team2Score;
    Button endMatchBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_match, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Sets the views/the layout to their specific variable
        headingRound = view.findViewById(R.id.tv_match_heading);
        player1 = view.findViewById(R.id.tv_match_playerPosition1);
        player2 = view.findViewById(R.id.tv_match_playerPosition2);
        player3 = view.findViewById(R.id.tv_match_playerPosition3);
        player4 = view.findViewById(R.id.tv_match_playerPosition4);
        team1Score = view.findViewById(R.id.etn_match_team1Score);
        team2Score = view.findViewById(R.id.etn_match_team2Score);
        endMatchBtn = view.findViewById(R.id.btn_match_setMatchScore);

        endMatchBtn.setEnabled(false);

        setUpRound(roundNo, ControlClass.getNumOfPlayers());

        //region TextListener - If team1Score and team2Score is equal matchscore. Enable step to end match! Integer.valueOf works instead of parseInt - Why? Dont know.
        team1Score.addTextChangedListener(new TextWatcher() {

            int totalScore1 = 0;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    totalScore1 = Integer.valueOf(team1Score.getText().toString()) + Integer.valueOf(team2Score.getText().toString());
                } catch (Exception e){

                }
                if(totalScore1 == ControlClass.getMatchScore()){
                    endMatchBtn.setEnabled(true);}
                else {
                    endMatchBtn.setEnabled(false);
                }
                }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        team2Score.addTextChangedListener(new TextWatcher() {

            int totalScore2 = 0;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    totalScore2 = Integer.valueOf(team1Score.getText().toString()) + Integer.valueOf(team2Score.getText().toString());
                } catch (Exception e){

                }
                if(totalScore2 == ControlClass.getMatchScore()){
                    endMatchBtn.setEnabled(true);}
                else {
                    endMatchBtn.setEnabled(false);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        //endregion

        endMatchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRoundScore(roundNo);
                addRound();
                team1Score.setText(null);
                team2Score.setText(null);
                endMatchBtn.setEnabled(false);
                setUpRound(roundNo, ControlClass.getNumOfPlayers());
            }
        });
    }

    public void setUpRound(int roundNoIn, int numberOfPlayersIn){
        switch (numberOfPlayersIn){
            case 4:
                if(roundNoIn == ControlClass.AmTOf4C1PP1.length){
                    team1Score.setEnabled(false);
                    team2Score.setEnabled(false);
                    if (this.getTag().equals("court1")){
                        endMatchBtn.setText("Court 1 done");
                    }
                    else if (this.getTag().equals("court2")){
                        endMatchBtn.setText("Court 2 done");
                    }
                    else if (this.getTag().equals("court3")){
                        endMatchBtn.setText("Court 3 done");
                    }
                }
                else if(this.getTag().equals("court1")){
                    headingRound.setText(String.format("Court 1; Round: %d", roundNoIn + 1));
                    player1.setText(ControlClass.getPlayer(ControlClass.AmTOf4C1PP1[roundNoIn]).getName());
                    player2.setText(ControlClass.getPlayer(ControlClass.AmTOf4C1PP2[roundNoIn]).getName());
                    player3.setText(ControlClass.getPlayer(ControlClass.AmTOf4C1PP3[roundNoIn]).getName());
                    player4.setText(ControlClass.getPlayer(ControlClass.AmTOf4C1PP4[roundNoIn]).getName());
                }
                break;
            case 5:
                if(roundNoIn == ControlClass.AmTOf5C1PP1.length){
                    team1Score.setEnabled(false);
                    team2Score.setEnabled(false);
                    if (this.getTag().equals("court1")){
                        endMatchBtn.setText("Court 1 done");
                    }
                    else if (this.getTag().equals("court2")){
                        endMatchBtn.setText("Court 2 done");
                    }
                    else if (this.getTag().equals("court3")){
                        endMatchBtn.setText("Court 3 done");
                    }
                }
                else if(this.getTag().equals("court1")){
                    headingRound.setText(String.format("Court 1; Round: %d", roundNoIn + 1));
                    player1.setText(ControlClass.getPlayer(ControlClass.AmTOf5C1PP1[roundNoIn]).getName());
                    player2.setText(ControlClass.getPlayer(ControlClass.AmTOf5C1PP2[roundNoIn]).getName());
                    player3.setText(ControlClass.getPlayer(ControlClass.AmTOf5C1PP3[roundNoIn]).getName());
                    player4.setText(ControlClass.getPlayer(ControlClass.AmTOf5C1PP4[roundNoIn]).getName());
                }
                break;
            case 8:
                if(roundNoIn == ControlClass.AmTOf8C1PP1.length){
                    team1Score.setEnabled(false);
                    team2Score.setEnabled(false);
                    if (this.getTag().equals("court1")){
                        endMatchBtn.setText("Court 1 done");
                    }
                    else if (this.getTag().equals("court2")){
                        endMatchBtn.setText("Court 2 done");
                    }
                    else if (this.getTag().equals("court3")){
                        endMatchBtn.setText("Court 3 done");
                    }
                }
                else if(this.getTag().equals("court1")){
                    headingRound.setText(String.format("Court 1; Round: %d", roundNoIn + 1));
                    player1.setText(ControlClass.getPlayer(ControlClass.AmTOf8C1PP1[roundNoIn]).getName());
                    player2.setText(ControlClass.getPlayer(ControlClass.AmTOf8C1PP2[roundNoIn]).getName());
                    player3.setText(ControlClass.getPlayer(ControlClass.AmTOf8C1PP3[roundNoIn]).getName());
                    player4.setText(ControlClass.getPlayer(ControlClass.AmTOf8C1PP4[roundNoIn]).getName());
                }
                else if(this.getTag().equals("court2")){
                    headingRound.setText(String.format("Court 2; Round: %d", roundNoIn  + 1));
                    player1.setText(ControlClass.getPlayer(ControlClass.AmTOf8C2PP1[roundNoIn]).getName());
                    player2.setText(ControlClass.getPlayer(ControlClass.AmTOf8C2PP2[roundNoIn]).getName());
                    player3.setText(ControlClass.getPlayer(ControlClass.AmTOf8C2PP3[roundNoIn]).getName());
                    player4.setText(ControlClass.getPlayer(ControlClass.AmTOf8C2PP4[roundNoIn]).getName());
                }
                break;
            case 12:
                if(roundNoIn == ControlClass.AmTOf12C1PP1.length){
                    team1Score.setEnabled(false);
                    team2Score.setEnabled(false);
                    if (this.getTag().equals("court1")){
                        endMatchBtn.setText("Court 1 done");
                    }
                    else if (this.getTag().equals("court2")){
                        endMatchBtn.setText("Court 2 done");
                    }
                    else if (this.getTag().equals("court3")){
                        endMatchBtn.setText("Court 3 done");
                    }
                }
                else if(this.getTag().equals("court1")){
                    headingRound.setText(String.format("Court 1; Round: %d", roundNoIn + 1));
                    player1.setText(ControlClass.getPlayer(ControlClass.AmTOf12C1PP1[roundNoIn]).getName());
                    player2.setText(ControlClass.getPlayer(ControlClass.AmTOf12C1PP2[roundNoIn]).getName());
                    player3.setText(ControlClass.getPlayer(ControlClass.AmTOf12C1PP3[roundNoIn]).getName());
                    player4.setText(ControlClass.getPlayer(ControlClass.AmTOf12C1PP4[roundNoIn]).getName());
                }
                else if(this.getTag().equals("court2")){
                    headingRound.setText(String.format("Court 2; Round: %d", roundNoIn  + 1));
                    player1.setText(ControlClass.getPlayer(ControlClass.AmTOf12C2PP1[roundNoIn]).getName());
                    player2.setText(ControlClass.getPlayer(ControlClass.AmTOf12C2PP2[roundNoIn]).getName());
                    player3.setText(ControlClass.getPlayer(ControlClass.AmTOf12C2PP3[roundNoIn]).getName());
                    player4.setText(ControlClass.getPlayer(ControlClass.AmTOf12C2PP4[roundNoIn]).getName());
                }
                else if(this.getTag().equals("court3")){
                    headingRound.setText(String.format("Court 3; Round: %d", roundNoIn  + 1));
                    player1.setText(ControlClass.getPlayer(ControlClass.AmTOf12C3PP1[roundNoIn]).getName());
                    player2.setText(ControlClass.getPlayer(ControlClass.AmTOf12C3PP2[roundNoIn]).getName());
                    player3.setText(ControlClass.getPlayer(ControlClass.AmTOf12C3PP3[roundNoIn]).getName());
                    player4.setText(ControlClass.getPlayer(ControlClass.AmTOf12C3PP4[roundNoIn]).getName());
                }
                break;
        }
    }

    public void addRoundScore(int roundNoIn){
        if(ControlClass.getNumOfPlayers() == 4) {
            if (this.getTag().equals("court1")) {
                ControlClass.getPlayer(ControlClass.AmTOf4C1PP1[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf4C1PP2[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf4C1PP3[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf4C1PP4[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
            }
        }
        else if(ControlClass.getNumOfPlayers() == 5) {
            if (this.getTag().equals("court1")) {
                ControlClass.getPlayer(ControlClass.AmTOf5C1PP1[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf5C1PP2[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf5C1PP3[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf5C1PP4[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
            }
        }
        else if(ControlClass.getNumOfPlayers() == 8) {
            if (this.getTag().equals("court1")) {
                ControlClass.getPlayer(ControlClass.AmTOf8C1PP1[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf8C1PP2[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf8C1PP3[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf8C1PP4[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
            }
            else if(this.getTag().equals("court2")){
                ControlClass.getPlayer(ControlClass.AmTOf8C2PP1[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf8C2PP2[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf8C2PP3[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf8C2PP4[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
            }
        }
        else if(ControlClass.getNumOfPlayers() == 12) {
            if (this.getTag().equals("court1")) {
                ControlClass.getPlayer(ControlClass.AmTOf12C1PP1[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf12C1PP2[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf12C1PP3[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf12C1PP4[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
            }
            else if(this.getTag().equals("court2")){
                ControlClass.getPlayer(ControlClass.AmTOf12C2PP1[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf12C2PP2[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf12C2PP3[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf12C2PP4[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
            }
            else if(this.getTag().equals("court3")){
                ControlClass.getPlayer(ControlClass.AmTOf12C3PP1[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf12C3PP2[roundNoIn]).addPlayerScore(Integer.parseInt(team1Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf12C3PP3[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
                ControlClass.getPlayer(ControlClass.AmTOf12C3PP4[roundNoIn]).addPlayerScore(Integer.parseInt(team2Score.getText().toString()));
            }
        }
    }

    public void addRound(){
        this.roundNo++;
    }

}